/** 
  * @desc Client Side Players render logic
  * 
*/

// Player class, and this will be used to render the players with needed data only
var Player = function(intiPacket) {
  var self = {};
  self.id = intiPacket.id;
  self.x = intiPacket.x;
  self.y = intiPacket.y;
  self.zoom = intiPacket.zoom;
  self.color = intiPacket.color;
  self.width = intiPacket.width;
  self.height = intiPacket.height;
  self.maxSpd = intiPacket.maxSpd,
  self.armor = intiPacket.armor,
  self.maxHealth = intiPacket.maxHealth,
  self.health = intiPacket.health,
  self.damage = intiPacket.damage,
  self.clip = intiPacket.clip,
  self.reload_time = intiPacket.reload_time,
  self.accuracy = intiPacket.accuracy,
  Player.list[self.id] = self;
  return self;
}

Player.list = {};

function renderPlayer(player) {
  //hp bar for the player
  var HP_BAR = 33 * player.health / player.maxHealth;
  noStroke();
  fill(player.color);
  rect(player.x - HP_BAR * 0.18, player.y - 18, HP_BAR, 2);
  //square
  noStroke();
  fill(player.color);
  rect(player.x, player.y, player.width, player.height);
  //ellipse
  strokeWeight(4);
  stroke(player.color);
  noFill();
  ellipse(
    player.x + (player.width / 2),
    player.y + (player.height / 2),
    player.width + (player.width - 10),
    player.height + (player.width - 10)
  );
}

/**
 * this renders the stats of the player
 * @param {object} player the stats object
 */
function renderPlayerStats(player) {
  const {accuracy, armor, clip, color, health, maxSpd, reload_time, zoom} = player;
  var playerDetails = `Accuracy: ${accuracy} \nArmor: ${armor} \nClip: ${clip} \nColor: ${color} \nHealth: ${health} \nSpeed: ${maxSpd} \nReload Time: ${reload_time} \nZoom: ${zoom}`;
  noStroke();
  fill(255);
  textSize(15);
  text(playerDetails, 20, 30); // Text wraps within text box
}


/**
 * EXPERIMENTAL: this will setup the user camera for the user and follow them accordingly based of player position
 * @param {object} player
 */
function renderCamera(player) {
  // console.log('player', player);  
  // camera(windowWidth / 2 - player.x / 2, windowHeight/ 2 - player.y / 2, 50, player.x, player.y, player.zoom, 0, 1, 0);
  // fill(51);
  // rect(0, 0, 50, 50); 

  // camera(player.x / 2, player.y / 2, player.zoom * 200 + 150, player.x, player.y, 0, 0, 1, 0);
  // plane(120, 120);

}