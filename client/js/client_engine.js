/** 
  * @desc logic that will be used to have the client interact with the server
  * 
*/

// Keep track of our socket connection
var socket;

function setup() {
  socket = io();

  // Canvas properties
  createCanvas(windowWidth, windowHeight);
  
  // translate(GAME_SETTINGS.VIEWPORT.widthHalf, GAME_SETTINGS.VIEWPORT.heightHalf);
  
  // Initial color of canvas
  background(GAME_SETTINGS.CANVAS.backgroundColor);

  // Get initial data from server
  init_entities();

  // Update the game scene
  update_entities();

  // listen if the server removes things from the view
  remove_entities();
  
  // Frame rate
  frameRate(GAME_SETTINGS.frame_rate);

  // This will listen for chat messages sent
  listenForChatMessages();
  
  //the player sends a message in the chat
  sendMessageToChat();
  
}

/**
 * Get all the players and create instances of them
 */
function init_entities() {
  // Data from server based on game event listener
  socket.on(GAME_SETTINGS.EVENTS.init_entities,
    (data) => {
      for(let i = 0; i < data.player.length; i++) {
        new Player(data.player[i]);
      }
      for(let i = 0; i < data.bullet.length; i++) {
        new Bullet(data.bullet[i]);
      }
    }
  );
}

/**
 * update the instances of players/bullets ect where needed
 */
function update_entities() {
  // Data from server based on game event listener
  socket.on(GAME_SETTINGS.EVENTS.update_entities,
    (data) => {
      for(let i = 0; i < data.players.length; i++) {
        let pack = data.players[i];
        let player = Player.list[pack.id];
        if(player) {
          if(pack.x !== undefined) {
            player.x = pack.x;
          }
          if(pack.y !== undefined) {
            player.y = pack.y;
          }
        }
      }

      for(let i = 0; i < data.bullets.length; i++) {
        let pack = data.bullets[i];
        let bullet = Bullet.list[pack.id];
        if(bullet) {
          if(pack.x !== undefined) {
            bullet.x = pack.x;
          }
          if(pack.y !== undefined) {
            bullet.y = pack.y;
          }
        }
      }
    }
  );
}

/**
 * Remove any thing that is not relevant to be shown on the server client side
 */
function remove_entities() {
  // Data from server based on game event listener
  socket.on(GAME_SETTINGS.EVENTS.remove_entities,
    (data) => {
      for(let i = 0; i < data.player.length; i++) {
        delete Player.list[data.player[i]];
      }
      for(let i = 0; i < data.bullet.length; i++) {
        delete Bullet.list[data.bullet[i]];
      }
    }
  );
}

/**
 * Get all the players and render them to on scene
 */
function renderSceneEntities() {
  socket.on(GAME_SETTINGS.EVENTS.get_current_user,
    (data) => {
      PLAYER.DETAILS = data;
      PLAYER.COLOR = data.colorOptions.indexOf(data.color);
    }
  );
  // Initial color of canvas
  background(GAME_SETTINGS.CANVAS.backgroundColor);
  // DEBUG GRID
  PRAGMATIC_TOOLS.drawGrid();
  // Render Players
  for(let i in Player.list) {
    renderPlayer(Player.list[i]);
    // make sure we only show stats for current player
    if(Player.list[i].id === PLAYER.DETAILS.id) {
      renderPlayerStats(Player.list[i]);
    }
    // Render Bullets
    for(let j in Bullet.list) {
      renderBullet(Bullet.list[j], PLAYER.DETAILS);
    }
  }

}

// draw that will update the client side frames
function draw() {
  renderSceneEntities();
}

/**
 * Update the canvas size if screen is resized
 */
function windowResized() {
  // P5 should natevly try and resize as its already imported
  resizeCanvas(windowWidth, windowHeight);
}
