/** 
  * @desc Client Side Bullets render logic
  * 
*/

// Bullets class, and this will be used to render the players with needed data only
var Bullet = function(intiPacket) {
  var self = {};
  self.id = intiPacket.id;
  self.x = intiPacket.x;
  self.y = intiPacket.y;
  self.color = intiPacket.color;
  self.width = intiPacket.width;
  self.height = intiPacket.height;
  Bullet.list[self.id] = self;
  return self;
}

Bullet.list = {};

function renderBullet(bullet, player) {
  //square
  noStroke();
  //set the color the same as the player
  fill(bullet.color);
  rect(
    bullet.x + player.width / 2 - bullet.width / 2,
    bullet.y + player.height / 2 - bullet.height / 2, 
    bullet.width, bullet.height);
 }