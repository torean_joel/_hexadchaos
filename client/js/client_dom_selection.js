/** 
  * @desc This is where we will have the DOM selectors that will get elements
  * and make it easier to use them on the engine side
  * 
*/

var _DOM_chat_box_wrapper = document.getElementById('chat-box-wrapper'), 
  _DOM_chat_text = document.getElementById('chat-messages'),
  _DOM_chat_input = document.getElementById('chat-input'),
  _DOM_chat_form = document.getElementById('chat-form');
