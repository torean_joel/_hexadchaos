/** 
  * @desc Helper methods that will manage and reuse code on the client side
  * 
*/
console.log('%c Client HELPERS LOADED', 'background: blue; color: white');

const PRAGMATIC_TOOLS = {
/**
 * This is a native js loop but this property jus expects data array
 * @param {array} array - The data that will be looped over
 * @param {func} callback - the method that will be called on each itteration
 */
  loop: (array, callback) => {
    for(let index = 0 ; index < array.length; index++) {
      callback(index);
    }
  },
  
  /**
   * This will draw a grid on the scene, helps with origin problems after translating
   */
  drawGrid: () => {
    const { width, height } = GAME_SETTINGS.VIEWPORT;

    stroke(50);
    strokeWeight(1);
    fill(255);
    for (let x =- width; x < width; x += 40) {
      line(x, -height, x, height);
      textSize(5);
      text(x, x + 1, 12);
    }
    for (let y =- height; y < height; y += 40) {
      line(-width, y, width, y);
      textSize(5);
      text(y, 1, y + 12, 15);
    }
  }
}
