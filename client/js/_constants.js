/**
  * @desc Game Settings constants that affect game initla values
 */

let GAME_SETTINGS = {
  frame_rate: 40,
  DEBUG_MODE: true,
  GAME_TEXT: {
    disconnected: 'You have been disconnecetd from the server'
  },
  // this should be the view of the user not the canvas
  VIEWPORT: {
    width: 1920,
    height: 1080,
    widthHalf: 1920 / 2,
    heightHalf: 1080 / 2
  },
  CANVAS: {
    backgroundColor: "#000",
    backgroundImage: "../assets/grid.jpg"
  },
  EVENTS: {
    get_current_user: 'get_current_user',
    get_player_messages: 'get_player_messages',
    send_message_to_players: 'send_message_to_players',
    debug_eval: 'debug_eval',
    debug_response: 'debug_response',
    get_players: 'get_players',
    get_bullets: 'get_bullets',
    init_entities: 'init_entities',
    update_entities: 'update_entities',
    remove_entities: 'remove_entities'
  }
}

/**
  * @desc chat options
 */
let CHAT_SETTINGS = {
  is_chat_open: false,
}

/**
  * @desc Player options
 */
const PLAYER = {
  DETAILS: {},
  MOVE: {
    up: {key: 87, value: 'up'},
    down: {key: 83, value: 'down'},
    left: {key: 65, value: 'left'},
    right: {key: 68, value: 'right'},
  },
  ATTACK: {
    cursor_angle: 'cursor_angle',
    normal_attack: 'normal_attack'
  },
  PRESSED: {
    enter: 13,
    right_ctrl: 17,
    space_bar: {key: 32, value: 'space_bar'},
  },
  EVENTS: {
    key_press: 'key_press',
  }
}

/**
  * @desc Server options that are avail for the client when
  * connecting to server - based off node port
 */
const SERVER_OPTIONS = {
  domain: window.location.hostname+':8001'
}

