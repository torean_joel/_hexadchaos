/** 
  * @desc This is where the logic and interaction for the chat of the system happens
  * 
*/

/**
 * This will listen for any messages sent from players in the game session
 */
function listenForChatMessages() {
  // Data from server based on game event listener
  socket.on(GAME_SETTINGS.EVENTS.get_player_messages,
    (data) => {
      _DOM_chat_text.innerHTML += '<div>' + data + '</div>';
      //update scroll height to latest message
      _DOM_chat_text.scrollTop = _DOM_chat_text.scrollHeight;
    }
  );

  //the debug response from the server evaluation
  socket.on(GAME_SETTINGS.EVENTS.debug_response,
    (data) => {
      console.log("%c SERVER DEBUG_RESPONSE: ", "font-size: 18px; color: red", data);
    }
  );
}

/**
* This will send a message if the player submits the chat form
*/
function sendMessageToChat() {
  _DOM_chat_form.onsubmit = function(e) {
    e.preventDefault();
    // here we check if the user is debugging or not
    if(_DOM_chat_input.value.indexOf('/debug ') === 0 && GAME_SETTINGS.DEBUG_MODE)
      socket.emit(GAME_SETTINGS.EVENTS.debug_eval, _DOM_chat_input.value.slice(7));
    else
      socket.emit(GAME_SETTINGS.EVENTS.send_message_to_players, _DOM_chat_input.value);
    
    _DOM_chat_input.value = "";
  }
}


/**
* Method to open and close the chat
*/
function toggleChatBox() {
  CHAT_SETTINGS.is_chat_open = !CHAT_SETTINGS.is_chat_open;
  if(CHAT_SETTINGS.is_chat_open) {
    _DOM_chat_box_wrapper.style = "display:block";
    _DOM_chat_input.focus();
  } else {
    _DOM_chat_box_wrapper.style = "display:none";
  }
}