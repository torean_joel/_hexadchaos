/** 
  * @desc The controls and updating user based off controls
  * 
*/

/**
 * Check if the client is pressing a key
 */
document.onkeydown = function (event) {
  const { keyCode } = event;
  console.log(keyCode);
  if(!CHAT_SETTINGS.is_chat_open) {
    if (keyCode === PLAYER.MOVE.up.key)
      socket.emit(PLAYER.EVENTS.key_press,
        { input_key: PLAYER.MOVE.up.value, state: true });
    if (keyCode === PLAYER.MOVE.down.key)
      socket.emit(PLAYER.EVENTS.key_press,
        { input_key: PLAYER.MOVE.down.value, state: true });
    if (keyCode === PLAYER.MOVE.left.key)
      socket.emit(PLAYER.EVENTS.key_press,
        { input_key: PLAYER.MOVE.left.value, state: true });
    if (keyCode === PLAYER.MOVE.right.key)
      socket.emit(PLAYER.EVENTS.key_press,
        { input_key: PLAYER.MOVE.right.value, state: true });
    if (keyCode === PLAYER.PRESSED.space_bar.key)
      socket.emit(PLAYER.PRESSED.key_press,
        { input_key: PLAYER.PRESSED.space_bar.value, state: true });
  }

  // here we want to see the user press ctrl + enter
  if (keyCode === PLAYER.PRESSED.right_ctrl) {
    toggleChatBox();
  }
}

/**
 * Check if the client stopped pressing a key
 */
document.onkeyup = function (event) {
  const { keyCode } = event;
  if (keyCode === PLAYER.MOVE.up.key)
    socket.emit(PLAYER.EVENTS.key_press,
      { input_key: PLAYER.MOVE.up.value, state: false });
  if (keyCode === PLAYER.MOVE.down.key)
    socket.emit(PLAYER.EVENTS.key_press,
      { input_key: PLAYER.MOVE.down.value, state: false });
  if (keyCode === PLAYER.MOVE.left.key)
    socket.emit(PLAYER.EVENTS.key_press,
      { input_key: PLAYER.MOVE.left.value, state: false });
  if (keyCode === PLAYER.MOVE.right.key)
    socket.emit(PLAYER.EVENTS.key_press,
      { input_key: PLAYER.MOVE.right.value, state: false });
  if (keyCode === PLAYER.PRESSED.space_bar.key)
    socket.emit(PLAYER.PRESSED.key_press,
      { input_key: PLAYER.PRESSED.space_bar.value, state: false });
}

/**
 * This will get the andlge and keep track of where the mouse is
 */
document.onmousemove = function (event) {
  var x = -PLAYER.DETAILS.x + (event.clientX - 8);
  var y = -PLAYER.DETAILS.y + (event.clientY - 8);
  var angle = Math.atan2(y, x) * 180 / Math.PI;
  
  socket.emit(PLAYER.EVENTS.key_press,
    { input_key: PLAYER.ATTACK.cursor_angle, state: angle });
}

/**
 * Check where the player clicked
 */
document.onmousedown = function () {
  socket.emit(PLAYER.EVENTS.key_press,
    { input_key: PLAYER.ATTACK.normal_attack, state: true });
}

/**
 * Check where the player released click
 */
document.onmouseup = function () {
  socket.emit(PLAYER.EVENTS.key_press,
    { input_key: PLAYER.ATTACK.normal_attack, state: false });
}