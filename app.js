/**
 * SERVER SETUP WITH HTTP/FILE SERVER MODULES
 * This will handel what files will be send to the client
 * and know how to handle them not allowed to use certian files
 */

// HTTP Portion
var express = require('express');
var app = express();
var server = require('http').Server(app);

// this needs to be turned off for production
var DEBUG_MODE = true;

//base player data
var PLAYER_BASE = {
  MAX_SPEED: 8,
  MAX_HEALTH: 600,
  MAX_ARMOR: 50, // percent
  RELOAD_TIME: 1.5, // seconds
  ACC: 1, // 0 - 1
  COOLDOWN: 180 //seconds
}

// this will make sure on initial route to domain, it sends user to clients index
app.get('/', function(req, res) {
 res.sendFile(__dirname + '/client/index.html');
});

// this allows use to get files in the client directory if we typed it in the url i.e assets folder
app.use('/', express.static(__dirname + '/client/'));

server.listen(process.env.PORT || 8001);

/**
 * WEBSOCKET SETUP WITH THE SERVER
 * Here we setup sockets and listen on the server for any emits and act
 * accordingly updating all clients involved
 */
var SOCKET_LIST = {},
  playerAllowedColors = ['#f44336', '#4caf50', '#2196f3', '#fff'];
  PLAYER_TYPES = {
    red: '#f44336',
    green: '#4caf50',
    blue: '#2196f3',
    white: '#fff'
  };
  PLAYER_COLORS = Object.keys(PLAYER_TYPES);

const Entity = function() {
  var self = {}

  self.update = function() {
    self.updatePosition();
  }

  self.updatePosition = function() {
    self.x += self.spdX;
    self.y += self.spdY;
  }

  self.getDistance = function(pt) {
    return Math.sqrt(Math.pow(self.x - pt.x, 2) + Math.pow(self.y-pt.y, 2));
  }

  return self;
}

const damageCalculation = function(player, shooter) {
  var playerArmorDeduction =  player.armor / 100; // percent
  var randomValueRange = function(min, max) {
    return  Math.floor(Math.random() * (max - min + 1)) + min;
  }
  
  switch (shooter.color) {
    case 'red':
      var dmg = randomValueRange(7, 11);
      return dmg  - (dmg * playerArmorDeduction);
    case 'green':
      var dmg = randomValueRange(5, 7);
      return dmg  - (dmg * playerArmorDeduction);
    case 'blue':
      return shooter.damage && shooter.damage - (shooter.damage * playerArmorDeduction);
    default:
      return shooter.damage && shooter.damage - (shooter.damage * playerArmorDeduction);
  }
}

const setPlayerColorAttr = function(self) {
  switch (self.color) {
    case 'red':
      /**
       * RED:
       *  
       * Attributes
       * -----
       * Speed: MAX_SPEED * .33
       * Armor: MAX_ARMOR * 0.33
       * Health: MAX_HEALTH
       * Damage: (7 - 11)
       * clip size: 10 shots before reload
       * Reload time: RELOAD_TIME
       * Accuracy: ACC - 20
       * vod: 2
       * 
       * Special
       * -----
       * Full Heal (600 max) - Heal - COOLDOWN
       * -----
       */
        self.maxSpd = PLAYER_BASE.MAX_SPEED * .85;
        self.armor = PLAYER_BASE.MAX_ARMOR * .33;
        self.maxHealth = PLAYER_BASE.MAX_HEALTH;
        self.health = self.maxHealth,
        self.clip = 10;
        self.reload_time = PLAYER_BASE.RELOAD_TIME;
        self.accuracy = PLAYER_BASE.ACC - .2;
        self.canShoot = true;
        self.zoom = 2;
      break;
    case 'green':
      /**
       * GREEN:
       * 
       * Attributes
       * -----
       * Speed: MAX_SPEED
       * Armor: MAX_ARMOR * 0.33
       * Health: MAX_HEALTH * 0.33
       * Damage: (5 - 7)
       * clip size: 25 shots before reload
       * Reload time: RELOAD_TIME
       * Accuracy: ACC - 40
       * vod: 1
       * 
       * Special
       * -----
       * Jump/teleport - LEAP (3 charges) - COOLDOWN
       * 
       */
        self.maxSpd = PLAYER_BASE.MAX_SPEED * .95;
        self.armor = PLAYER_BASE.MAX_ARMOR * .33;
        self.maxHealth = PLAYER_BASE.MAX_HEALTH * .33;
        self.health = self.maxHealth,
        self.clip = 25;
        self.reload_time = PLAYER_BASE.RELOAD_TIME;
        self.accuracy = PLAYER_BASE.ACC - .3;
        self.canShoot = true;
        self.zoom = 1;
      break;
    case 'blue':
      /**
       * BLUE:
       * 
       * Attributes
       * -----
       * Speed: MAX_SPEED * 0.33
       * Armor: MAX_ARMOR
       * Health: MAX_HEALTH * 0.33
       * Damage: 42
       * clip size: 2 shots before reload
       * Reload time: RELOAD_TIME + RELOAD_TIME
       * Accuracy: Depends how many hit
       * vod: 3
       * 
       * Special
       * -----
       * ARMOR / DAMAGE increase (*2 both) - 5seconds - COOLDOWN
       * 
       * ------------------------------------------------------
       */
        self.maxSpd = PLAYER_BASE.MAX_SPEED * .85;
        self.armor = PLAYER_BASE.MAX_ARMOR;
        self.maxHealth = PLAYER_BASE.MAX_HEALTH * .33;
        self.health = self.maxHealth,
        self.damage = 42;
        self.clip = 2;
        self.reload_time = PLAYER_BASE.RELOAD_TIME * 2;
        self.accuracy = PLAYER_BASE.ACC - .4;
        self.canShoot = true;
        self.zoom = 3;
      break;
    default:
      /**
       * WHITE (With Flag):
       * 
       * Attributes
       * -----
       * Speed: MAX_SPEED 
       * Armor: MAX_ARMOR
       * Health: MAX_HEALTH
       * vod: 3 - we can see over walls as a white player
       * 
       * DURATION: 1 min
       * 
       * WHITE (WITHOUT Flag):
       * 
       * Attributes
       * -----
       * Speed: MAX_SPEED * .3
       * Armor: MAX_ARMOR * .3
       * Health: MAX_HEALTH * .3
       * Damage: 5
       * clip size: 6 shots before reload
       * Reload time: RELOAD_TIME - 0.5
       * Accuracy: ACC
       * vod: 3 - we can see over walls as a white player
       * 
       * DURATION: No Limit
       */
        if(!self.hasFlag) {
          self.maxSpd = PLAYER_BASE.MAX_SPEED * .85;
          self.armor = PLAYER_BASE.MAX_ARMOR * .3;
          self.maxHealth = PLAYER_BASE.MAX_HEALTH * .3;
          self.health = self.maxHealth,
          self.damage = 5;
          self.clip = 6;
          self.reload_time = PLAYER_BASE.RELOAD_TIME * .5;
          self.accuracy = PLAYER_BASE.ACC;
          self.zoom = 3;
          self.canShoot = true;
        } else {
          self.maxSpd = PLAYER_BASE.MAX_SPEED;
          self.armor = PLAYER_BASE.MAX_ARMOR;
          self.maxHealth = PLAYER_BASE.MAX_HEALTH
          self.health = self.maxHealth,
          self.canShoot = false;
          self.zoom = 3;
        }
      break;
  }
}

const Player = function(id) {
  // here we will create a new instance of the entity
  var self = Entity();
    // player id
    self.id = id;
    // player position
    self.x = Math.floor(Math.random() * 600);
    self.y = Math.floor(Math.random() * 600);
    // player is moving
    self.pressedRight = false;
    self.pressedLeft = false;
    self.pressedUp = false;
    self.pressedDown = false;
    self.pressedSpace = false,
    // player is aim angle
    self.cursorAngle = 0;
    // player is attacking
    self.pressedNormalAttack = false;
    // player attributes
    self.width = 20;
    self.height = 20;
    // player will be able to change this themselves
    self.color = PLAYER_COLORS[Math.floor(Math.random() * PLAYER_COLORS.length)];
    self.colorOptions = PLAYER_COLORS;

    setPlayerColorAttr(self);
  // create a reference of the entity update loop
  var super_update = self.update;
  self.update = function() {
    self.updateSpd();
    super_update();

      //this is to generate a random bullet
      if(self.pressedNormalAttack) {
        self.shootBullet(self.cursorAngle);
      }
  }

  self.shootBullet = function(angle) {
    var bullet = Bullet(self.id, self.color, angle);
        bullet.x = self.x;
        bullet.y = self.y;
        bullet.color = self.color;
  }

  self.updateSpd = function() {
    if(self.pressedRight)
      self.spdX = self.maxSpd;
    else if(self.pressedLeft)
      self.spdX = -self.maxSpd;
    else
      self.spdX = 0;

    if(self.pressedUp)
      self.spdY = -self.maxSpd;
    else if(self.pressedDown)
      self.spdY = self.maxSpd;
    else
      self.spdY = 0;
  }

  self.getInitPack = function() {
    return {
      id: self.id,
      x: self.x,
      y: self.y,
      zoom: self.zoom,
      color: self.color,
      colorOptions: self.colorOptions,
      width: self.width,
      height: self.height,
      maxSpd: self.maxSpd,
      armor: self.armor,
      maxHealth: self.maxHealth,
      health: self.health,
      damage: self.damage,
      clip: self.clip,
      reload_time: self.reload_time,
      accuracy: self.accuracy,
    }
  }

  self.getUpdatePack = function() {
    return {
      id: self.id,
      x: self.x,
      y: self.y,
      zoom: self.zoom,
      color: self.color,
      width: self.width,
      height: self.height,
      maxSpd: self.maxSpd,
      armor: self.armor,
      health: self.health,
      damage: self.damage,
      clip: self.clip,
      reload_time: self.reload_time,
      accuracy: self.accuracy,
    }
  }

  Player.list[id] = self;
  initPack.player.push(self.getInitPack());
  return self;
};

Player.list = {};
Player.onConnect = function(socket) {
  var player = Player(socket.id);
  socket.emit('get_current_user', player);

  socket.on('key_press', function(data) {
    if(data.input_key === 'up')
      player.pressedUp = data.state;
    if(data.input_key === 'down')
      player.pressedDown = data.state;
    if(data.input_key === 'left')
      player.pressedLeft = data.state;
    if(data.input_key === 'right')
      player.pressedRight = data.state;
    if(data.input_key === 'space_bar')
      console.log('trying to change color');
      player.pressedSpace = data.state;
    if(data.input_key === 'normal_attack')
      player.pressedNormalAttack = data.state;
    if(data.input_key === 'cursor_angle')
      player.cursorAngle = data.state;

    socket.emit('get_current_user', player);

    socket.emit('init_entities', {
      player: Player.getAllInitPack(),
      bullet: Bullet.getAllInitPack(),
    });
  });
}
Player.getAllInitPack = function() {
  var players = [];
    for(var i in Player.list)
      players.push(Player.list[i].getInitPack());
  return players;
}
Player.onDisconnect = function(socket) {
  delete Player.list[socket.id];
  removePack.player.push(socket.id);
}
Player.update = function() {
  var pack = [];
  // get all the data and position updates of each player instance
  for(let i in Player.list) {
    var player = Player.list[i];
    player.update();
    pack.push(player.getUpdatePack());
  }
  return pack
}

//bullets class
const Bullet = function(playerId, playerColor, angle) {
  var self = Entity();
  self.id = Math.random();
  self.spdX = Math.cos(angle/180*Math.PI) * 20;
  self.spdY = Math.sin(angle/180*Math.PI) * 20;
  self.timer = 0;
  self.ownerId = playerId;
  self.color = playerColor;
  self.toRemove = false;
  self.width = 8;
  self.height = 8;
  
  var super_update = self.update;
  self.update = function() {
    if(self.timer++ > 30)
      self.toRemove = true;
    super_update();

    //remove the bullet if it collides with another player, but not self
    for(let i in Player.list) {
      let player = Player.list[i];
      if(self.getDistance(player) < 32 && self.ownerId !== player.id) {
        // here we need to handle the remove of the player health
        var shooter = Player.list[self.ownerId];
        var damage = damageCalculation(player, shooter);
        console.log('player ', player.color, 'took', damage, 'damage from player', shooter.color);
        player.health -= damage;
        if(player.health <= 0) {
          player.health = player.maxHealth;
          // respawn at same start location
          player.x =  Math.floor(Math.random() * 600);
          player.y =  Math.floor(Math.random() * 600);
        }

        self.toRemove = true;
      }
    }
  }
  self.getInitPack = function() {
    return {
      id: self.id,
      x: self.x,
      y: self.y,
      width: self.width,
      height: self.height,
      color: self.color,
    }
  }
  self.getUpdatePack = function() {
    return {
      id: self.id,
      x: self.x,
      y: self.y,
      width: self.width,
      height: self.height,
      color: self.color,
    }
  }
  Bullet.list[self.id] = self;
  initPack.bullet.push(self.getInitPack());
  return self;
}
Bullet.list = {};
Bullet.update = function() {
  var pack = [];
  // get all the data and position updates of each bullet instance
  for(let i in Bullet.list) {
    var bullet = Bullet.list[i];
    //remove if the bullets travels a certain distance else add them to scene
    if(bullet.toRemove) {
      delete Bullet.list[i];
      removePack.bullet.push(bullet.id);
    } else {
      pack.push(bullet.getUpdatePack());
    }
    bullet.update();
  }
  return pack
}
Bullet.getAllInitPack = function() {
  var bullets = [];
  for(var i in Bullet.list)
    bullets.push(Bullet.list[i].getInitPack());
  return bullets;
}


var io = require('socket.io').listen(server);
// Register a callback function to run when we have an individual connection
// This is run for each individual user that connects
io.sockets.on('connection',
  // We are given a websocket object in our function
  function (socket) {
    socket.id = Math.random();
    SOCKET_LIST[socket.id] = socket;
    
    Player.onConnect(socket);

    socket.on('disconnect', function() {
      delete SOCKET_LIST[socket.id];
      Player.onDisconnect(socket);
    });
    

    //player sent a message
    socket.on('send_message_to_players', function(data) {
      var playerName = ("" + socket.id).slice(2,7);
      for(let i in SOCKET_LIST) {
        SOCKET_LIST[i].emit('get_player_messages', playerName + ': ' + data);
      }
    });

    //player sent a debug request debug_eval
    socket.on('debug_eval', function(data) {
      // there is a constant that needs to be set to false
      // before deploying to dissallow debugging in game
      if(!DEBUG_MODE)
        return;

      var response = eval(data);

      for(let i in SOCKET_LIST) {
        socket.emit('debug_response', response);
      }
    });
  }
);

var initPack = {
  player: [],
  bullet: []
};

var removePack = {
  player: [],
  bullet: []
};

// every x frames we want to loop through all connections
// and then send updates to all clients
setInterval(function() {
  var pack = {
    players: Player.update(),
    bullets: Bullet.update()
  };

  for(let i in SOCKET_LIST) {
    var socket = SOCKET_LIST[i];
    socket.emit('init_entities', initPack);
    socket.emit('update_entities', pack);
    socket.emit('remove_entities', removePack);
  }

  //set it to empty every frame
  initPack.player = [];
  initPack.bullet = [];
  removePack.player = [];
  removePack.bullet = [];

  // server will update and send to clients every x frames
}, 1000/45);